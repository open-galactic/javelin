# Groundstation-Tools - Javelin 

Ground station tools and utilities for allowing handling of satellite data, satellite control, ground station control and radio components. These tools provide the capability to send GraphQL commands to a spacecraft running the KubOS operating system. All code necessary for encoding the commands and transmitting them through a radio link are included in this repository, along with the capability to send commands directly over a network to an engineering model of the spacecraft.


Contains:

- `bbb_radio_emulator/`: Radio Emulator for BeagleBone Black
- `data/`: Data file structure for Command and Control
- `file-storage/`: File Storage for Command and Control
- `flowgraphs/`: GNU-radio config file
- `logs/`: Logs + file structure for Command and Control
- `rig_utils/`: Utility functions for SatComms
- `rotor_utils/`: Utility functions for ground station rotation control
- `scripts/`: Hamlib scripts ground station control
- `source/`: Ground station control source code and configuration
- `beacon_client`: Python script for receiving beacons
- `ftp_*`: Python scripts for FTP (File Transfer Protocol) commands
- `run_radio.py`: Python script for GNU Radio
- `send_cmp_script.py`: Python script for Command and Control



Requires:

- Python 3 (Recommended 3.9)
- GNU-Radio - https://wiki.gnuradio.org/index.php/InstallingGR


## Development

### Initial config


Setup & configure python:

```
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Clone `example.tle.txt` to `tle.txt` file in the root dir and update with desired TLE for tracking. 

# Command and Control


## Hardware Set-Up

The ground station software has four modes of operation. Direct network access to a BeagleBone black running Kubos, Radio Emulator link over IP with Cubesat Space Protocol, RF Link with Cubesat Space Protocol, and Full RF tracking.

### GraphQL over the network

The only thing during setup here is to set up the network appropriately. In our example code, the IP address of the host machine the BBB is connected to is set to 10.0.2.21 and the IP address of the BBB is 10.0.2.20.

### RF Emulator Link

A example script has been included in bbb_radio_emulator/. This has to be run on the BBB and can then take messages in the CSP packet format. Most of the examples in the repository follow this format. Development using this link is useful as it allows for proper design of IO data.

### RF Link

This should be reserved as a staging tool. Once your applications and GraphQL queries are set and tested using the emulator, you should use a lab setup or engineering model of your satellite to test the relevent parts. A GNURadio flowgraph has been included for a generic BPSK downlink, FSK uplink satellite with CSP.

### Full Tracking Mode

When your satellite is launched this repository contains full utilities to autonomously track and send data to your spacecraft.

## Setting up ports

In order to send commands to the correct service on the spacecraft, a dictionary called port_mapper is used which maps service names to port numbers. This dictionary is found in source/tools/commands/utils.py

The default port mapping is as follows:
```
port_mapper = {
    'app': 8000,
    'monitor': 8030,
    'scheduler': 8060,
    'telem': 8006
}
```
When you add new services to your kubos installation, (e.g. an adcs service), you can add it into this dictionary to allow you to send commands to that service.

Modify this dictionary so that it contains all of the services on your spacecraft, mapped to the correct ports. Note that FTP is not included, since it is not accessed using the standard GraphQL interface used by other services.

## Sending Commands

If everything has been set up correctly, then you should now be able to send commands to your spacecraft. Commands are sent by preparing a script file, which is then given as an argument to the send_cmd_script.py python program. If you have a script called "ping.txt" in the commandscripts directory, then you can send that script to the spacecraft using the following command:
```
python3 send_cmd_script.py commandscripts/ping.txt
```
The commands will be read one at a time from the script file. If no response is received from the spacecraft, then the program will resend the command, with default values of 5 retries, with 5 seconds wait between each retry (modification of these defaults is explained below.)

Regardless of whether a command succeeds or fails, the program will run the next command in the script file, going in order until every command has been sent.

## Script File Syntax

### Whitespace and comments

A script file consists of multiple lines, each of which can contain a command. Blank lines are ignored, as are lines beginning with #, which can be used as comments
```
# This line, and the next line, will be ignored by the program, and won't be sent to the spacecraft
   
```

We will now discuss the different types of commands that can be sent. Note that a command must be written on a single line, it cannot be split across multiple lines.

### GraphQL commands

In Kubos, most commands to the spacecraft are carried out using GraphQL to send a query or mutation to the desired service. An in depth explanation of how to use GraphQL to interact with Kubos can be found in the [Kubos documentation](https://docs.kubos.com/1.21.0/index.html).

A simple example of a script file that sends a GraphQL command is as follows:
```
# Sending a ping to the application service
graphql app query{ping}
```
The 'graphql' at the beginning indicates that we are sending a GraphQL command, and must be included. 'app' indicates which service to send the command to; in this case we are sending it to the application service. The service name must be included, and must be one of the services listed in the port_mapper dictionary discussed in the [Setting up ports](#setting-up-ports) section.

The GraphQL command itself comes after the service name. In this case, we are sending a ping query to the application service.

It is possible to modify the timeout behaviour of a command as follows:
```
# Sending a ping to the telemetry service, with a timeout of 2 seconds, and 3 attempted retries
graphql telem query{ping} -t 2 -r 3
```
This command is the same as the previous one, except this time we are sending the ping to the telemetry service, instead of the application service. We are also specifying that we want the program to wait only 2 seconds before resending the command (using the -t flag), and we want it to retry sending the command 3 times in total (using the -r flag).

A more complicated GraphQL command is as follows:
```
# Register the scheduler-test-app application to the application service, and return the application entry, along with errors and a success flag
graphql app mutation{register(path:\"home/system/file-storage/scheduler-test-app\"){errors, success, entry{app{name,executable}}}}
```
This command registers a new application with the application service, which is located at the specified location on the spacecraft. Note that any strings included in a GraphQL command must be surrounded by escaped double quotes (\\").

#### Error checking

The graphql command system contains some minimal error checking code, which can be found in the file source/tools/commands/gqlerrorcheck.py. As it is, this code only checks to make sure that a response was received, and that the topmost level of this response does not contain an 'errors' key. This function can be modified to better suit your needs, by checking to make sure that unexpected results are not returned. If you want to stop the execution of the script, simply raise an Exception, and the program will stop.

### FTP Commands

To transfer a file from the ground station to the spacecraft or vice versa, the ftp command is used. It has the following format:
```
ftp uplink sourcefilepath destinationfilepath
ftp downlink sourcefilepath destinationfilepath
```

The first word of an ftp command must be "ftp." The second word indicates whether we are uplinking a file to the spacecraft ("uplink") or downlinking a file from the spacecraft to the ground ("downlink"). After this, the first argument is the location of the file to be transferred (the source filepath), and the second argument is the location where the file is to be sent to. Both arguments are mandatory for an uplink command, but for a downlink command, the destination file path can be omitted. If it is, then the file will be stored in the file-storage/downlinkedfiles folder.

An example downlink command is:
```
ftp downlink \"/home/system/log/kubos-info.log\"
```
This command takes the file kubos-info.log, stored in the indicated location on the spacecraft, and transfers it from the spacecraft to the ground station, and into the file-storage/downlinkedfiles folder. Note that once again, the filepath (which is a string) must be enclosed in escaped double quotes.

### Delay Commands

There may be occasions where you want to wait a specified period of time before sending the next command. In order to accomplish this, a Delay command can be used, which has a very simple syntax:
```
delay 10
```
When run, this command will wait 10 seconds before moving on to the next command

### Example Script File

Below is an example script file called "registerapp.txt," which can be found in the javelin-scripts repository. It demonstrates all of the commands described in this section.

```
# This script uplinks and registers a simple application to the spacecraft.
# The application is stored in the scheduler-test folder in this directory.
# You will need to make sure that the source filepath matches the location
# of the scheduler-test folder on your computer. You will also need to make
# sure that the destination filepath exists on the spacecraft.

# Uplink the manifest file
ftp uplink \"/home/user/ground-station-client-prerelease/commandscripts/scheduler-test/manifest.toml\" \"/home/system/file-storage/scheduler-test-app/manifest.toml\"

# Uplink the application file
ftp uplink \"/home/user/ground-station-client-prerelease/commandscripts/scheduler-test/scheduler-test-app\" \"/home/system/file-storage/scheduler-test-app/scheduler-test-app\"

# This command is simply to demonstrate the delay command. It is not necessary,
# since it should be possible to register an application immediately after the
# file transfer is complete
delay 2

# Register the application
graphql app mutation{register(path:\"/home/system/file-storage/scheduler-test-app\"){errors, success, entry{app{name,executable}}}}
```
