#!/usr/bin/env python3

import json
import socketserver
import time

import requests

command_host = "0.0.0.0"
command_port = 8888


class MyUDPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]

        # space packet headers
        primary_header = data[:6]
        secondary_header = data[6:16]

        # cmd_id to be sent back to gs
        cmd_id = secondary_header[:8]
        payload = data[16:]

        # realistic delay
        time.sleep(1.0 / 9600 * len(payload) * 8)
        time.sleep(0.047)

        port = 256 * int(secondary_header[8]) + int(secondary_header[9])
        url = "http://0.0.0.0:{}".format(port)

        try:
            r = requests.post(url, json=json.loads(payload))
            print(r.status_code)
            print(r.text)
            payload = bytearray(r.text.encode("utf-8"))

        except Exception as e:
            payload = bytearray(str(e).encode())
            payload = json.dumps({"error": payload})
            print(payload)

        payload_len = len(payload) + 10

        # realistic delay
        time.sleep(1.0 / 9600 * len(payload) * 8)
        time.sleep(0.047)

        port_header = [port // 256, port % 256]
        payload_len_header = [payload_len // 256, payload_len % 256]

        response = (
            bytearray(primary_header[:4])
            + bytearray(payload_len_header)
            + bytearray(cmd_id)
            + bytearray(port_header)
            + payload
        )

        # send graphqp response back to groundstation
        socket.sendto(response, self.client_address)


if __name__ == "__main__":
    HOST, PORT = command_host, command_port
    with socketserver.UDPServer((HOST, PORT), MyUDPHandler) as server:
        server.serve_forever()
