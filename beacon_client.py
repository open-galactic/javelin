import json
import logging
import socketserver
from datetime import datetime

from source.tools.beacon import decode_msg, write_to_csv

filename = "data/beacon/beacon_test_" + datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S") + ".csv"
logfilename = "data/beacon/beacon_test_log-DEBUG" + datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S") + ".log"

logger = logging.getLogger("BEACON TEST")
hdlr = logging.FileHandler(logfilename)
formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)


class MyTCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        self.data = self.request.recv(1024).strip()
        timestamp = datetime.utcnow().isoformat()

        logger.debug(str(self.data))

        hex_data = self.data[16:].decode("ascii")
        logger.debug(hex_data)

        beacon_data = bytearray.fromhex(self.data[16:].decode("ascii"))
        beacon_spec, beacon_name, beacon_dict = decode_msg(beacon_data)
        logger.debug(json.dumps(beacon_dict))

        write_to_csv(filename, timestamp, beacon_name, beacon_spec, beacon_dict)


if __name__ == "__main__":
    HOST, PORT = "127.0.0.1", 8030

    with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
        server.serve_forever()
