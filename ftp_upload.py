#!/usr/bin/python3

import subprocess
import sys
import time
from datetime import datetime

from source.config import local_ip, local_port, remote_ip, remote_port

"""
GLOBAL OPTIONS USED BY BOTH SERVICES
"""
timeout = 60.0  # radio running in minutes before shutting down
kubos_ftp_log_output = False  # see output from kubos ftp client

"""
Options that user should not change
"""
subcommand = "upload"  # ['download','upload','cleanup]
chunk_size = 140
inter_chunk_delay = 500


def main(local_filepath, sat_filepath):
    ftp_client_args = [
        "kubos-file-client",
        "--remote-ip=" + str(remote_ip),
        "--remote-port=" + str(remote_port),
        "--host-ip=" + str(local_ip),
        "--host-port=" + str(local_port),
        "--transfer-chunk-size=" + str(chunk_size),
        "--inter-chunk-delay=" + str(inter_chunk_delay),
        "--max-chunks-transmit=" + str(100),
        subcommand,
    ]
    ftp_client_args.append(local_filepath)
    ftp_client_args.append(sat_filepath)

    start_time = time.perf_counter()
    print("start time: ", start_time)

    kubos_ftp_logfile_name = "logs/kubos_ftp/upload/{}.log".format(datetime.utcnow().isoformat())
    LOG_KUBOS = open(kubos_ftp_logfile_name, "w")
    if not kubos_ftp_log_output:
        ftp_client = subprocess.Popen(ftp_client_args)
    else:
        ftp_client = subprocess.Popen(ftp_client_args, stdout=LOG_KUBOS, stderr=subprocess.STDOUT)

    while ftp_client.poll() is None:
        print("running time: ", time.perf_counter() - start_time)
        time.sleep(1.0)

    ftp_client.terminate()


if __name__ == "__main__":
    _, local_filepath, sat_filepath = sys.argv

    print("FTP UPLOAD: (local file)   ", local_filepath)
    print("          : (satellite dir)", sat_filepath)

    main(local_filepath, sat_filepath)
