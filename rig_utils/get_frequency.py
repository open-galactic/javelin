from source.tools.control import ControlRig

rigctl = ControlRig()

if __name__ == "__main__":
    upfreq = rigctl.get_uplink_frequency()
    downfreq = rigctl.get_downlink_frequency()
    print(f"upfreq : {upfreq}, downfreq : {downfreq}")
