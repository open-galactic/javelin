import sys

from source.tools.control import ControlRig

rigctl = ControlRig()

if __name__ == "__main__":
    _, uplink_freq, downlink_freq = sys.argv
    print(f"uplink : {uplink_freq}, downlink : {downlink_freq}")
    rigctl.set_uplink_frequency(uplink_freq)
    rigctl.set_downlink_frequency(downlink_freq)
