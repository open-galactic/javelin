from source.tools.control import ControlRot

rotctl = ControlRot()

if __name__ == "__main__":
    az, el = rotctl.get_position()
    print(f"az : {az}, el : {el}")
