import sys

from source.tools.control import ControlRot

rotctl = ControlRot()

if __name__ == "__main__":
    _, az, el = sys.argv
    print(f"ax : {az}, el : {el}")
    rotctl.set_position(az, el)
