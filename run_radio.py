#!/usr/bin/python3

import atexit
import os
import subprocess
import sys
import time
from datetime import datetime

"""
GLOBAL OPTIONS USED BY BOTH SERVICES
"""
gnuradio_output = True  # see output from gnuradio including error messages

"""
GNURADIO OPTIONS!!!
"""
gnuradio_args = ["flowgraphs/transceiver.grc"]


def main(timeout):
    FNULL = open(os.devnull, "w")
    if not gnuradio_output:
        radio = subprocess.Popen(gnuradio_args, stdout=FNULL, stderr=subprocess.STDOUT)
    else:
        radio = subprocess.Popen(gnuradio_args)

    start_time = time.perf_counter()

    @atexit.register
    def terminate():
        """
        SHUTDOWN PROCESSES SAFELY
        """
        radio.terminate()

        print("shutting down radio.")

    while (time.perf_counter() - start_time) / 60.0 < timeout:
        time.sleep(1.0)

    radio.terminate()


if __name__ == "__main__":
    try:
        _, timeout = sys.argv
        timeout = float(timeout)
    except Exception:
        timeout = 30.0

    main(timeout)
