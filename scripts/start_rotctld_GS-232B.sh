#! /bin/sh

rotctld --model=603 --rot-file=/dev/ttyUSB0 \
	--listen-addr=127.0.0.1 --port=4533 \
	--serial-speed=9600 -vvvvv -C serial_handshake=Hardware \
	-C post_write_delay=10 -C timeout=1000 -C retry=0 
