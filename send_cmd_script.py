import os
import sys
import time

import ftp_download
import ftp_upload
from source.tools.commands.decode_gql import decode_gql
from source.tools.commands.gqlerrorcheck import errorcheck
from source.tools.commands.radio_handler import send_packet
from source.tools.commands.validate_delay import validate_delay
from source.tools.commands.validate_ftp import validate_ftp


def execute_operator_cmds(cmd_lines):
    for cmd in cmd_lines:
        cmd_arr = cmd.split()

        if cmd_arr[0] == "graphql":
            _, space_pkt_cmds, timeout, retry = decode_gql(cmd_arr)

            data = send_packet(space_pkt_cmds, timeout, retry)

            errorcheck(data)

        elif cmd_arr[0] == "ftp":
            validate_ftp(cmd)
            if len(cmd_arr) == 3:
                sourcefilepath = cmd_arr[2][2:-2]
                destfilepath = None
            else:
                sourcefilepath, destfilepath = cmd_arr[2:4]
                sourcefilepath = sourcefilepath[2:-2]
                destfilepath = destfilepath[2:-2]
                finaldir, finalname = os.path.split(destfilepath)

            if cmd_arr[1] == "downlink":
                oldname = os.path.basename(sourcefilepath)
                # You can change the temporary directory below if you want
                tempdir = "."
                templist = os.listdir(tempdir)
                if not destfilepath:
                    finaldir = "./file-storage/downlinkedfiles"
                    finalname = oldname

                if os.path.realpath(finaldir) == os.path.realpath(tempdir):
                    raise Exception(
                        "The final location of the file cannot be"
                        " the same as the temporary storage location "
                        f"{os.path.realpath(tempdir)}"
                    )

                if oldname in templist:
                    raise Exception(
                        f"The file {oldname} already exists in the "
                        f"temporary directory {tempdir}. Try deleting the file."
                    )

                success = False

                while not success:
                    # Attempt to downlink the file
                    ftp_download.main(sourcefilepath)

                    # Need to regenerate the templist since there should be
                    # a new file in it
                    templist = os.listdir(tempdir)
                    # Check that the file has been downlinked to the temporary
                    # directory
                    if oldname in templist:
                        # File is present, move it to final location
                        os.rename(f"{tempdir}/{oldname}", f"{finaldir}/{finalname}")
                        success = True
                    else:
                        # File not present, need to try again
                        success = False

            elif cmd_arr[1] == "uplink":
                ftp_upload.main(sourcefilepath, destfilepath)

        elif cmd_arr[0] == "delay":
            validate_delay(cmd)
            duration = int(cmd_arr[1])

            for t in range(duration, 0, -1):
                print(f"Pausing: {t}")
                time.sleep(1)

        else:
            raise Exception(f"Unknown command type {cmd_arr[0]}")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        raise ValueError("Please include exactly 1 argument, the filepath of" " the script to be executed.")

    with open(sys.argv[1], "r") as f:
        cmd_lines = []
        for line in f:
            stripped_line = line.strip()
            # Ignore comments and empty lines
            if stripped_line and stripped_line[0] != "#":
                cmd_lines.append(stripped_line)

    execute_operator_cmds(cmd_lines)
