# groundstation position
longitude = 151.2054
latitude = -33.893
altitude = 0.0

# commands ip and port settings
command_ip = "10.0.2.20"
command_port = 8888
# command_ip = '127.0.0.1'
# command_port = 8021

# ftp ip and port settings
remote_ip = "10.0.2.20"
remote_port = 8040
# remote_ip = '127.0.0.1'
# remote_port = 8040
local_ip = "10.0.2.21"
local_port = 8040
# local_ip = '127.0.0.1'
# local_port = 8040

# groundstation view above horizon
threshold = 10.0

# antenna settings - Rotor
rotate = False

# radio settings - SDR
radio_hardware_offset = 0
radio_hardware_gain = -1

# save local copies of recorded wavfiles
# set to True (utc iso datetime naming convention)
# set to False - ONLY THE LATEST OVERPASS WILL BE STORED LOCALLY
save_local = True
