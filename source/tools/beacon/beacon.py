import json
import os
import struct

from math import floor, ceil


def import_beacon_spec(id_no):
    with open("source/tools/beacon/beacon-format.json", "r") as f:
        beacon_specs = json.load(f)
    for name, beacon in beacon_specs.items():
        if beacon["id"] == id_no:
            return beacon_specs[name], name
    raise Exception("Unknown beacon ID number")


def write_to_csv(filename, timestamp, beacon_name, beacon_spec, beacon_data):
    if not os.path.isfile(filename):
        with open(filename, "w") as f:
            headers = ",".join([x["name"] for x in beacon_spec["fields"]])
            headers = "timestamp(iso)," + headers
            f.write(headers + "\n")
    with open(filename, "a") as f:
        value_str = ",".join([beacon_data[field["name"]] for field in beacon_spec["fields"]])
        value_str = timestamp + "," + value_str
        f.write(value_str + "\n")


def twos_complement(value, size):
    if value & (1 << (size - 1)):
        value -= 1 << size
    return value


def signed_mag(value, size):
    if value & (1 << (size - 1)):
        value = -(value ^ (1 << size - 1))
    return value


def decode_msg_check(msg_bytes):
    id_no = msg_bytes[0]
    beacon_spec, beacon_name = import_beacon_spec(id_no)

    beacon_data = {}
    byte_count = 0
    for field in beacon_spec["fields"]:
        value = int.from_bytes(msg_bytes[floor(byte_count) : ceil(byte_count + field["size"] // 8)], "big")
        beacon_data[field["name"]] = value
        byte_count += field["size"] / 8

    return beacon_data


def decode_msg(msg_bytes):
    id_no = msg_bytes[0]
    beacon_spec, beacon_name = import_beacon_spec(id_no)
    msg = int.from_bytes(msg_bytes, "big")
    beacon_data = {}
    bit_count = sum([x["size"] for x in beacon_spec["fields"]])

    for field in beacon_spec["fields"]:

        bit_count -= field["size"]
        value = (msg >> bit_count) & ((1 << field["size"]) - 1)

        if field["type"] == "uint":
            beacon_data[field["name"]] = str(value)
        elif field["type"] == "int":
            beacon_data[field["name"]] = str(signed_mag(value, field["size"]))
        elif field["type"] == "float":
            beacon_data[field["name"]] = struct.unpack("f", struct.pack("I", value))[0]
        elif field["type"] == "double":
            beacon_data[field["name"]] = struct.unpack("d", struct.pack("I", value))[0]
        elif field["type"] == "bool":
            beacon_data[field["name"]] = str(value)
        elif field["type"] == "hex":
            beacon_data[field["name"]] = "{:x}".format(value)
        else:
            beacon_data[field["name"]] = str(value)

    [print(k + ":", v) for k, v in beacon_data.items()]
    print("actual beacon len =", len(msg_bytes) * 8, "bits")
    print(
        "expect beacon len = ", sum([x["size"] for x in beacon_spec["fields"] if x["name"] != "test"]), "bits"
    )
    print("padding =", beacon_spec["fields"][-1]["size"])

    return beacon_spec, beacon_name, beacon_data


def read_from_file():
    with open("beacon_data.txt", "r") as f:
        data_bin = []
        for i, line in enumerate(f):
            if i % 2 == 1:
                data_bin.append(bytearray.fromhex(line[66:-2]))
    return data_bin


if __name__ == "__main__":
    # beacon_data = read_from_file()
    byte_msg = bytearray.fromhex(
        "0100002e5e011731157c0d771d6e00020007000000b80008005b0000000000020000010b0000000d0019001a00190018000000000001f67c000000010101000003070200520014060a01370041024900010898089d07a10a31060801360041024900010898089d003f1000000000000000000000000000000000000032c4cf59b05064003b7a1b53ed06d26baa230000000000000000010000010001010000000000000001000000000000000000010000010100000001010000010000000000000001000000000000000000000001000100000002000000010001000000020000"
    )
    payload_data = decode_msg(byte_msg)
