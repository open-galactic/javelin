from .gen_cmd import encode_space_pkt
from .utils import port_mapper


def decode_gql(cmd_arr):
    system = cmd_arr[1]
    port = port_mapper.get(system)
    if isinstance(port, type(None)):
        raise Exception("Unknown port for system {}".format(system))

    cmd_arr = cmd_arr[2:]  # Remove the 'graphql' and system markers

    try:
        timeout_index = cmd_arr.index("-t")
        # Run the next 2 lines twice to remove the timeout elements from the
        # array
        timeout = cmd_arr.pop(timeout_index)
        timeout = float(cmd_arr.pop(timeout_index))
    except ValueError:
        timeout = 5

    try:
        retry_index = cmd_arr.index("-r")
        # Run the next 2 lines twice to remove the retry elements from the
        # array
        retry = cmd_arr.pop(timeout_index)
        retry = int(cmd_arr.pop(timeout_index))
    except ValueError:
        retry = 5

    gql_str = "".join(cmd_arr)
    gql_str = '{"query": "' + gql_str + '"}'
    encoded_cmd, _ = encode_space_pkt(gql_str, port)

    return gql_str, encoded_cmd, timeout, retry
