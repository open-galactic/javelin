from .spacePkt.encode_space_packet import encode


def encode_space_pkt(gql_str, port):
    cmd_id = 0
    payload_type = 0
    sequence = 0
    encoded_cmd, sequence = encode(cmd_id, payload_type, port, gql_str, sequence)
    return encoded_cmd, sequence
