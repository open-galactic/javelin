# This function can be used to check any returned packets for errors, and
# handle them accordingly. In its current form, it simply checks that there is
# nothing in the graphql 'errors' field, and it checks that something was
# received back from the spacecraft (rather than nothing)

import json

from .spacePkt.decode_space_packet import decode


def errorcheck(data):

    if len(data) > 0:
        decoded_data = decode(data)
        gql_resp = json.loads(decoded_data[3])

        if "errors" in gql_resp.keys():
            raise Exception("Command returned the following error:\n\n" f"{gql_resp['errors']}")
    else:
        raise Exception("Command failed to return a response. Exiting..")
