import json
import logging
from datetime import datetime

from source.config import command_ip, command_port
from source.tools.utils import setup_logger

from .comms import Commsocket
from .spacePkt.decode_space_packet import decode

host = command_ip
port = command_port

setup_logger("commands", "logs/commands/command.log")
command_logger = logging.getLogger("commands")


def send_packet(cmd_pkts, timeout, retry):
    data = []

    gnuradio_socket = Commsocket(host, port, timeout=timeout)

    for cmd_pkt in cmd_pkts:
        got_data = False
        for j in range(int(retry)):
            print("sending pkt", cmd_pkt)

            # log the sent command
            command_logger.info(
                "cmd : "
                + str(cmd_pkt[16:].decode())
                + ", num. message sent : "
                + str(j)
                + ", num. bytes : "
                + str(len(cmd_pkt))
                + " - message sent : "
                + cmd_pkt.hex()
            )
            t0 = datetime.utcnow()
            response = gnuradio_socket.send(cmd_pkt)

            # log the response
            if len(response) > 0:
                command_logger.info(
                    "message received : "
                    + str(j)
                    + ", num bytes : "
                    + str(len(response))
                    + ", response time : "
                    + str((datetime.utcnow() - t0).total_seconds())
                    + " - message received : "
                    + response.hex()
                )
            else:
                command_logger.info("message received : NONE")
                command_logger.error("failed to receive message, sent message " + str(cmd_pkt[16:].decode()))

            print("got back", response)

            if len(response) > 0:
                data.append(response)
                got_data = True

                command_logger.info("Raw response data:" + str(data))
                decoded_data = decode(data)
                gql_resp = json.loads(decoded_data[3])
                print("\n")
                dat = next(iter(gql_resp.values()))
                if isinstance(dat, dict):
                    print(json.dumps(dat, sort_keys=True, indent=4))
                else:
                    print(dat)
                break

        if not got_data:
            command_logger.warn(
                "failed to receive a response after multiple attempts. \n\tcmd sent: "
                + str(cmd_pkt[16:].decode())
                + "\n\traw hex: "
                + str(cmd_pkt.hex())
            )
            print("failed to recieve a response!!\n Command was:\n {}".format(cmd_pkt))
            break

    return data
