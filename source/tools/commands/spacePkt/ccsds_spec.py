max_user_data_field_size = 65536
packet_ver = 0
primary_header_fields = (
    ("packet_version", 3),
    ("packet_type", 1),
    ("second_header_flag", 1),
    ("APID", 11),
    ("sequence_flags", 2),
    ("sequence_count", 14),
    ("packet_data_length", 16),
)

secondary_header_fields = ()

payload_header_fields = (("command_id", 64), ("destination_port", 16))
