import random
import string

from . import encode_space_packet
from .ccsds_spec import (
    payload_header_fields,
    primary_header_fields,
    secondary_header_fields,
)


def random_cmd_generator(size, chars=string.printable):
    return "".join(random.choice(chars) for x in range(size))


def get_primary_header(header_bin):
    header_int = int.from_bytes(header_bin, "big")
    primary_header_data = {}
    bit_count = sum([x[1] for x in primary_header_fields])
    for header_field in primary_header_fields:
        bit_count -= header_field[1]
        data = (header_int >> bit_count) & ((1 << header_field[1]) - 1)
        primary_header_data[header_field[0]] = data
    return primary_header_data


def get_secondary_header(header_bin):
    header_int = int.from_bytes(header_bin, "big")
    bit_count = sum([x[1] for x in secondary_header_fields])
    secondary_header_data = {"length": bit_count // 8}
    for header_field in secondary_header_fields:
        bit_count -= header_field[1]
        data = (header_int >> bit_count) & ((1 << header_field[1]) - 1)
        print(data)
        secondary_header_data[header_field[0]] = data
    return secondary_header_data


def get_payload_header(header_bin):
    header_int = int.from_bytes(header_bin, "big")
    bit_count = sum([x[1] for x in payload_header_fields])
    payload_header_data = {"length": bit_count // 8}
    for header_field in payload_header_fields:
        bit_count -= header_field[1]
        data = (header_int >> bit_count) & ((1 << header_field[1]) - 1)
        payload_header_data[header_field[0]] = data
    return payload_header_data


def decode(space_packets):
    primary_headers = []
    for space_packet in space_packets:
        assert type(space_packet) == bytes
        assert len(space_packet) >= 6

        primary_header_len = sum([x[1] for x in primary_header_fields]) // 8
        secondary_header_len = sum([x[1] for x in secondary_header_fields]) // 8
        payload_header_len = sum([x[1] for x in payload_header_fields]) // 8

        primary_header = get_primary_header(space_packet[0:primary_header_len])
        # Note Kubos does not appear to respect the CCSDS spec here. It uses
        # the full length of the payload, instead of the payload length - 1
        assert len(space_packet) - primary_header_len == primary_header["packet_data_length"]  # + 1
        primary_headers.append(primary_header)

        if primary_header["second_header_flag"]:
            secondary_data_field = space_packet[
                primary_header_len : primary_header_len + secondary_header_len
            ]
            secondary_header = get_secondary_header(secondary_data_field)
        else:
            secondary_header = None

        payload_data_field_bin = space_packet[
            primary_header_len
            + secondary_header_len : primary_header_len
            + secondary_header_len
            + payload_header_len
        ]
        payload_header = get_payload_header(payload_data_field_bin)
        payload_header_len = payload_header["length"]

        user_data_field_bin = space_packet[
            primary_header_len
            + secondary_header_len
            + payload_header_len : primary_header_len
            + primary_header["packet_data_length"]
            + 1
        ]
        user_data_field = user_data_field_bin.decode("utf-8")
        del payload_header["length"]

    return primary_headers, secondary_header, payload_header, user_data_field


if __name__ == "__main__":
    cmd_id = 0
    payload_type = 0  # GraphQL
    port = 8021

    payload = """{\"query\":\"{testping {success}}\"}"""

    packet_sequence_count = 0
    space_packets, new_sequence_count = encode_space_packet.encode(
        cmd_id, payload_type, port, payload, packet_sequence_count
    )
    (primary_headers, secondary_headers, payload_header, user_data_field) = decode(space_packets)

    encoded_msg = ", ".join(str(x) for x in space_packets[0])

    if 1:
        print("original message:\n" + payload + "\n")
        print("encoded message:\n" + encoded_msg + "\n")
        print("decoded message:\n" + user_data_field + "\n")
        print("decoded primary headers:" + str(primary_headers) + "\n")
        print("decoded payload headers:" + str(payload_header))

    assert payload == user_data_field
    # Naive assersion, assumed no secondary header
    assert new_sequence_count == packet_sequence_count + -(-len(payload) // 65536)
