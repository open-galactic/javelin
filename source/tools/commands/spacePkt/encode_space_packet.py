from .ccsds_spec import (
    max_user_data_field_size,
    packet_ver,
    payload_header_fields,
    primary_header_fields,
)


def get_primary_header(header_data):
    primary_header = 0
    for header_field in primary_header_fields:
        primary_header = (primary_header << header_field[1]) + header_data[header_field[0]]
    return primary_header.to_bytes(6, "big")


def get_secondary_header():
    # No secondary header for Kubos
    return None


def get_encoded_user_data_field(payload_str):
    assert type(payload_str) == str
    user_data_field = payload_str.encode("utf-8")
    return user_data_field


def get_payload_header(header_data):
    payload_header = 0
    for header_field in payload_header_fields:
        payload_header = (payload_header << header_field[1]) + header_data[header_field[0]]
    return payload_header.to_bytes(10, "big")


def encode(cmd_id, payload_type, port, payload, packet_sequence_count):
    user_data_field = get_encoded_user_data_field(payload)

    payload_header_data = {"command_id": cmd_id, "destination_port": port}
    payload_header = get_payload_header(payload_header_data)
    total_packet_data_field = payload_header + user_data_field

    no_of_req_packets = -(-len(total_packet_data_field) // max_user_data_field_size)

    secondary_header = get_secondary_header()

    # Kubos ignores the sequence flags. It appears to use 0x00 for all types
    # of packets
    primary_header_data = {
        "packet_version": packet_ver,
        "packet_type": 0,
        "second_header_flag": 0,
        "APID": payload_type,
        #'sequence_flags': 0x03,
        "sequence_flags": 0x00,
    }
    if secondary_header:
        primary_header_data["second_header_flag"] = 0x01
    if no_of_req_packets > 1:
        primary_header_data["sequence_flags"] = 0x01

    space_packets = []
    for packet_no in range(no_of_req_packets):
        packet_data_field = total_packet_data_field[
            packet_no * max_user_data_field_size : (packet_no + 1) * max_user_data_field_size
        ]
        # Kubos does not appear to respsect the CCSDS spec here. It uses the
        # full length of the payload, instead of the payload length - 1
        primary_header_data["packet_data_length"] = len(packet_data_field)  # -1
        primary_header_data["sequence_count"] = packet_no + packet_sequence_count
        if packet_no > 0:
            if packet_no == no_of_req_packets - 1:
                primary_header_data["sequence_flags"] = 0x02
            else:
                primary_header_data["sequence_flags"] = 0x00
        primary_header = get_primary_header(primary_header_data)
        space_packet = primary_header + packet_data_field
        space_packets.append(space_packet)

    packet_sequence_count += no_of_req_packets
    return space_packets, packet_sequence_count
