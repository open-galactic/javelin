from decode_space_packet import decode
from encode_space_packet import encode

cmd_id = 0
payload_type = 0  # GraphQL defined as id 0, UDP = ID 1
port = 8021
payload = """{"query":"mutation {epsWatchdogKick {success}}"}"""
sequence = 0

msg, sequence = encode(cmd_id, payload_type, port, payload, sequence)

if 0:
    delineator = " "
    hex_msg = delineator.join("{:02x}".format(x) for x in encoded_msg[0])
    print("hex message = ", hex_msg)

    decimal_msg = int.from_bytes(encoded_msg[0], "big", signed=False)
    print("decimal message = ", decimal_msg)

    int_msg = " ".join(str(x) for x in encoded_msg[0])
    print("int array = ", int_msg)

# msg = bytes([0, 0, 0, 0, 0, 57, 0, 0, 0, 0, 0, 0, 0, 0, 31, 85, 123, 34, 113, 117, 101, 114, 121, 34, 58, 34, 109, 117, 116, 97, 116, 105, 111, 110, 32, 123, 101, 112, 115, 87, 97, 116, 99, 104, 100, 111, 103, 75, 105, 99, 107, 32, 123, 115, 117, 99, 99, 101, 115, 115, 125, 125, 34, 125])
# print(len(msg))
msg = [
    bytes(
        [
            0,
            1,
            0,
            0,
            0,
            42,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            123,
            34,
            113,
            117,
            101,
            114,
            121,
            34,
            58,
            34,
            123,
            116,
            101,
            115,
            116,
            112,
            105,
            110,
            103,
            123,
            115,
            117,
            99,
            99,
            99,
            101,
            115,
            115,
            125,
            125,
            34,
            125,
        ]
    )
]
print(msg)

primary_headers, secondary_headers, user_data_field = decode(msg)
print(primary_headers, "\n", user_data_field)
