# Modify these mappings to reflect the port mappings for your spacecraft

port_mapper = {"app": 8000, "monitor": 8030, "scheduler": 8060, "telem": 8006}

datatype_mapper = {"String": str, "Int": int, "Float": float, "Boolean": bool}
