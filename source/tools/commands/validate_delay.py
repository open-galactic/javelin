import re


def validate_delay(usr_str):
    usr_str = usr_str.strip()
    # The below regex expression can be best understood as 4 separate groups
    # which are as follows:      {1}{  2  }|{  3  }|{  4  }|{   5   }
    # Bracket guide:         ((      [     ]| [  ]  | [  ]  |  [  ]   ) )
    cmd_pat = re.compile(r"""((?:\\ |[^ "'[]|"[^"]*"|'[^']*'|\[[^]]*\])+)""")
    cmd_arr = cmd_pat.split(usr_str)[1::2]

    if len(cmd_arr) != 2:
        raise Exception("Invalid number of arguments for Delay command. " "Expect 1")

    try:
        duration = int(cmd_arr[1])
        if duration <= 0:
            raise Exception
    except:
        raise ValueError(
            "The delay value must be a positive integer. " f"A value of {cmd_arr[1]} was provided"
        )
