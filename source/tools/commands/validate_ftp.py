import re


def validate_ftp(usr_str):
    usr_str = usr_str.strip()
    # The below regex expression can be best understood as 4 separate groups
    # which are as follows:      {1}{  2  }|{  3  }|{  4  }|{   5   }
    # Bracket guide:         ((      [     ]| [  ]  | [  ]  |  [  ]   ) )
    cmd_pat = re.compile(r"""((?:\\ |[^ "'[]|"[^"]*"|'[^']*'|\[[^]]*\])+)""")
    cmd_arr = cmd_pat.split(usr_str)[1::2]

    if cmd_arr[1] == "uplink":
        # An uplink command must have 2 arguments: a source and destination
        # filepath
        if len(cmd_arr) != 4:
            raise Exception(f"Invalid number of arguments for FTP uplink" "command. Expect 2")
    elif cmd_arr[1] == "downlink":
        # A downlink command must have either 1 or 2 arguments. The source
        # filepath is required, the destination is optional
        if len(cmd_arr) not in [3, 4]:
            raise Exception(f"Invalid number of arguments for FTP downlink" "command. Expect 1 or 2")
    else:
        raise Exception(f"Invalid param {cmd_arr[1]}")

    # Check that filepaths are enclosed in escaped double quotes
    for filepath in cmd_arr[2:]:
        if filepath.startswith('\\"') and filepath.endswith('\\"'):
            pass
        else:
            raise Exception(
                f"""Invalid filepath {filepath}. Filepaths """
                """must begin and end with escaped double quotes (\\")"""
            )
