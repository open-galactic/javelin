import logging
import os
import pprint
import sched
import subprocess
import time
from datetime import datetime, timedelta
from multiprocessing import Process

from dateutil.parser import parse

from source.config import save_local
from source.tools.tracking.refine import refine_overpass
from source.tools.utils import setup_logger, to_abs_local
from source.tools.utils.waterfall import plot_waterfall

from .radio import radio_info
from .rig_control import ControlRig
from .rot_control import ControlRot


class GroundstationControl:
    def __init__(self):
        self.running_processes = {}

        setup_logger("control", "logs/control/control.log")
        self.control_logger = logging.getLogger("control")
        self.control_logger.info("started hardware controller")

        # create the rig/rot controller
        self.rotctl = ControlRot()
        self.rigctl = ControlRig()

    def plan_track(
        self,
        norad_id,
        cmd_id,
        downlink_frequency,
        uplink_frequency,
        gnuradio_args,
        iq_filename,
        start_time,
        maximum_elevation_time,
        end_time,
        line0,
        line1,
        line2,
        azel,
        doppler,
        timestamps,
        setpoints,
    ):

        s = sched.scheduler(time.time, time.sleep)

        def start_gnuradio():
            self.control_logger.info("starting radio")
            if gnuradio_args is not None:
                logfile_name = "gnuradio_log_{}.log".format(sorted(timestamps)[0])
                gnuradio_logfile = open(os.path.join("logs/gnuradio/", logfile_name), "w")

                self.running_processes[cmd_id]["gnuradio_process"] = subprocess.Popen(
                    gnuradio_args, stdout=gnuradio_logfile, stderr=subprocess.STDOUT
                )

        def stop_gnuradio():
            self.control_logger.info("stutting down radio")
            if gnuradio_args is not None:
                self.running_processes[cmd_id]["gnuradio_process"].terminate()

        def setup_overpass():
            azimuth, elevation = setpoints[0]

            self.control_logger.info(
                "setup rotor for overpass: " + str(azimuth) + " az, " + str(elevation) + " el"
            )
            self.rotctl.set_position(azimuth, elevation)

        def start_overpass():
            start_gnuradio()

        def end_overpass():
            stop_gnuradio()
            time.sleep(5.0)

            self.control_logger.info("park rotor: " + str(0.0) + " az, " + str(0.0) + " el")
            time.sleep(5)
            self.rotctl.set_position(180.0, 0.0)

            # generate waterfall plot for adhoc analysis
            time.sleep(10.0)
            plot_waterfall(iq_filename)

        # set up the rotor to initial position
        s.enterabs(to_abs_local(parse(sorted(timestamps)[0]) - timedelta(minutes=2)), 1, setup_overpass)

        # start the overpass - turn on flowgraph
        s.enterabs(to_abs_local(parse(sorted(timestamps)[0]) - timedelta(minutes=1)), 1, start_overpass)

        # stop the overpass - turn off flowgraph / save data / upload data to cloud
        s.enterabs(to_abs_local(parse(sorted(timestamps)[-1])), 1, end_overpass)

        # doppler correction
        for timestamp, shift in zip(*refine_overpass(timestamps, doppler)):
            corrected_downlink_frequency = (1.0 - shift) * downlink_frequency
            corrected_uplink_frequency = (1.0 - shift) * uplink_frequency

            s.enterabs(
                timestamp,
                1,
                self.rigctl.set_downlink_frequency,
                kwargs={"frequency": corrected_downlink_frequency},
            )
            s.enterabs(
                timestamp,
                1,
                self.rigctl.set_uplink_frequency,
                kwargs={"frequency": corrected_uplink_frequency},
            )
            s.enterabs(timestamp, 1, self.rigctl.get_downlink_frequency)
            s.enterabs(timestamp, 1, self.rigctl.get_uplink_frequency)

        # set the rotor positions
        for timestamp, (azimuth, elevation) in zip(timestamps, setpoints):
            timestamp = parse(timestamp)

            s.enterabs(to_abs_local(timestamp), 1, self.rotctl.stop_motion)
            s.enterabs(
                to_abs_local(timestamp),
                1,
                self.rotctl.set_position,
                kwargs={"azimuth": azimuth, "elevation": elevation},
            )
            s.enterabs(to_abs_local(timestamp), 1, self.rotctl.get_position)

        # print information of the overpass to terminal
        for timestamp, (azimuth, elevation), shift in zip(timestamps, azel, doppler):
            timestamp = parse(timestamp)

            corrected_downlink_frequency = (1.0 - shift) * downlink_frequency
            corrected_uplink_frequency = (1.0 - shift) * uplink_frequency

            s.enterabs(
                to_abs_local(timestamp),
                1,
                self.print_info,
                kwargs={
                    "timestamp": timestamp,
                    "azimuth": azimuth,
                    "elevation": elevation,
                    "downlink_frequency": corrected_downlink_frequency,
                    "uplink_frequency": corrected_uplink_frequency,
                    "doppler": shift,
                },
            )

        s.run()

    def print_info(self, timestamp, azimuth, elevation, downlink_frequency, uplink_frequency, doppler):
        pprint.pprint(
            {
                "timestamp": timestamp.strftime("%Y-%m-%dT%H:%M:%S"),
                "azimuth": azimuth,
                "elevation": elevation,
                "downlink_frequency": downlink_frequency,
                "uplink_frequency": uplink_frequency,
                "doppler": doppler,
            }
        )

    def command_parser(self, norad_id, cmd_id, overpass):

        start_time = overpass["start_time"]
        maximum_elevation_time = overpass["maximum_elevation_time"]
        end_time = overpass["end_time"]
        tle = overpass["tle"]
        line0 = tle["line0"]
        line1 = tle["line1"]
        line2 = tle["line2"]
        azel = overpass["azel"]
        doppler = overpass["doppler"]
        timestamps = overpass["timestamps"]
        setpoints = overpass["setpoints"]

        overpass_str = "\n"
        overpass_str += "current time: " + datetime.utcnow().isoformat() + "\n"
        overpass_str += "start time: " + start_time + "\n"
        overpass_str += "maximum_elevation_time" + maximum_elevation_time + "\n"
        overpass_str += "end time" + end_time + "\n"
        overpass_str += line0 + "\n" + line1 + "\n" + line2

        self.control_logger.info(overpass_str)

        # if a local copy of the overpass iq wavfile is required then
        # timestamp the file. if the file is not required, only the latest
        # overpass will be stored locally.
        if save_local:
            iq_filename = "data/iq_files/overpass_" + start_time.split(".")[0] + ".wav"
        else:
            iq_filename = "data/iq_files/overpass.wav"

        # extract stored satellite information and return gnuradio
        # arguments to pass to the control
        satellite_info, gnuradio_args = radio_info(norad_id, iq_filename)
        self.control_logger.info(gnuradio_args)

        # get centre frequency for overpass from stored satellite
        # information
        downlink_frequency = satellite_info["downlink_frequency"]
        uplink_frequency = satellite_info["uplink_frequency"]

        track_sched = Process(
            target=self.plan_track,
            args=(
                norad_id,
                cmd_id,
                downlink_frequency,
                uplink_frequency,
                gnuradio_args,
                iq_filename,
                start_time,
                maximum_elevation_time,
                end_time,
                line0,
                line1,
                line2,
                azel,
                doppler,
                timestamps,
                setpoints,
            ),
        )

        self.running_processes[cmd_id] = {
            "control_process": track_sched,
            "gnuradio_process": None,
            "start_time": start_time,
            "end_time": end_time,
        }
        self.running_processes[cmd_id]["control_process"].start()
