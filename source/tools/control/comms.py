import socket


class socketComms(object):
    buffer_size = 2048
    tasks_buffer_size = 10480
    connected = False

    def __init__(self, ip_address, port):
        self.ip_address = ip_address
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    def connect(self):
        try:
            self.sock.connect((self.ip_address, self.port))
            self.connected = True
        except socket.error:
            self.connected = False
        return self.connected

    def send(self, message):
        if not self.connected:
            self.connect()
        try:
            self.sock.send(message.encode("ascii"))
        except socket.error:
            pass
        response = self.sock.recv(self.tasks_buffer_size).decode("ascii")
        return response

    def send_not_recv(self, message):
        if not self.is_connected:
            self.connect()
        self.sock.send(message.encode("ascii"))

    def receive(self, size):
        resp = self.sock.recv(size)
        return resp
