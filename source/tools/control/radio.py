def radio_info(norad_id, iq_filename):
    downlink_freq = 437075000
    uplink_freq = 145875000

    satellite_dict = {"downlink_frequency": downlink_freq, "uplink_frequency": uplink_freq, "name": "EXAMPLE"}

    gnuradio_args = [
        "flowgraphs/doppler_transceiver.py",
        "--wav-file=" + str(iq_filename),
        "--centre-freq-rx=" + str(downlink_freq),
    ]

    return satellite_dict, gnuradio_args
