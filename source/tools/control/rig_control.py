import logging

from source.tools.utils import setup_logger

from .comms import socketComms


class ControlRig:
    def __init__(self):
        localhost = "127.0.0.1"
        rigctl_downlink_port = 4532
        rigctl_uplink_port = 4534

        setup_logger("rigctl", "logs/rigctl/rigctl.log")
        self.rigctl_logger = logging.getLogger("rigctl")
        self.rigctl_downlink_socket = socketComms(localhost, rigctl_downlink_port)
        self.rigctl_uplink_socket = socketComms(localhost, rigctl_uplink_port)
        self.rigctl_downlink_socket.connect()
        self.rigctl_uplink_socket.connect()
        self.rigctl_logger.info("connected")

    def set_downlink_frequency(self, frequency):
        msg = "F {0}\n".format(int(frequency))
        data = self.rigctl_downlink_socket.send(msg)
        self.rigctl_logger.info("downlink - " + msg.rstrip("\n"))
        self.rigctl_logger.info("downlink - " + data.rstrip("\n"))

    def set_uplink_frequency(self, frequency):
        msg = "F {0}\n".format(int(frequency))
        data = self.rigctl_uplink_socket.send(msg)
        self.rigctl_logger.info("uplink - " + msg.rstrip("\n"))
        self.rigctl_logger.info("uplink - " + data.rstrip("\n"))

    def get_downlink_frequency(self):
        msg = "f\n"
        data = self.rigctl_downlink_socket.send(msg)
        self.rigctl_logger.info("downlink - " + msg.rstrip("\n"))
        self.rigctl_logger.info("downlink - " + data.rstrip("\n"))
        return int(data.rstrip("\n"))

    def get_uplink_frequency(self):
        msg = "f\n"
        data = self.rigctl_uplink_socket.send(msg)
        self.rigctl_logger.info("uplink - " + msg.rstrip("\n"))
        self.rigctl_logger.info("uplink - " + data.rstrip("\n"))
        return int(data.rstrip("\n"))
