import logging

from source.tools.utils import setup_logger

from .comms import socketComms


class ControlRot:
    def __init__(self):
        localhost = "127.0.0.1"
        rotctl_port = 4533

        setup_logger("rotctl", "logs/rotctl/rotctl.log")
        self.rotctl_logger = logging.getLogger("rotctl")
        self.rotctl_socket = socketComms(localhost, rotctl_port)
        self.rotctl_socket.connect()
        self.rotctl_logger.info("connected")

    def set_position(self, azimuth, elevation):
        msg = "P {0} {1}\n".format(int(azimuth), int(elevation))
        data = self.rotctl_socket.send(msg).rstrip("\n")
        self.rotctl_logger.info(msg.rstrip("\n"))
        self.rotctl_logger.info(data)

    def get_position(self):
        msg = "p\n"
        data = self.rotctl_socket.send(msg)
        self.rotctl_logger.info(msg.rstrip("\n"))
        self.rotctl_logger.info(data)

        azimuth, elevation, _ = data.split("\n")
        return float(azimuth), float(elevation)

    def stop_motion(self):
        msg = "S\n"
        data = self.rotctl_socket.send(msg)
        self.rotctl_logger.info(msg.rstrip("\n"))
        self.rotctl_logger.info(data)
