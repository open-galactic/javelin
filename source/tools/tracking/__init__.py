from .calculate import overpass_leapfrog_path
from .overpass import overpass_time_info


class Tracker:
    def __init__(self, longitude, latitude, altitude, threshold):
        self.longitude = longitude
        self.latitude = latitude
        self.altitude = altitude
        self.threshold = threshold

    def overpass_time_info(self, query_time, line0, line1, line2, period=24):
        return overpass_time_info(
            query_time,
            line0,
            line1,
            line2,
            self.longitude,
            self.latitude,
            self.altitude,
            self.threshold,
            period,
        )

    def overpass_leapfrog_path(self, query_time, line0, line1, line2, period=24):
        return overpass_leapfrog_path(
            query_time,
            line0,
            line1,
            line2,
            self.longitude,
            self.latitude,
            self.altitude,
            self.threshold,
            period,
        )
