from datetime import timedelta

import numpy as np
from pyorbital.astronomy import observer_position
from pyorbital.orbital import Orbital

from .constants import KM_TO_M, SPEED_OF_LIGHT
from .error import cosine_error
from .overpass import overpass_time_info
from .setpoints import get_setpoints


def overpass_leapfrog_path(query_time, line0, line1, line2, longitude, latitude, altitude, threshold, period):
    """
    breaks up a uniform second-by-second overpass
    into a uniform error list (5 degree cosine error beteen
    each az-el setpoint).

    Keyword arguments:
    query_datetime -- the initial datetime object for the search
    line0 -- tle line 0
    line1 -- tle line 1
    line2 -- tle line 2
    longitude -- the observers longitude
    latitude -- the observers latitude
    altitude -- the observers altidue
    period -- length of the search window for overpasses
    threshold -- degrees above the horizon for satellite overpasses to include
    """

    overpass_list_dict = []

    # define an orbital object given the tle
    try:
        orb = Orbital(line0, line1=line1, line2=line2)

        overpass_list = overpass_time_info(
            query_time, line0, line1, line2, longitude, latitude, altitude, threshold, period
        )

        for rise_time, fall_time, max_el_time in overpass_list:
            overpass_dict = dict(
                start_time=rise_time.isoformat(),
                maximum_elevation_time=max_el_time.isoformat(),
                end_time=fall_time.isoformat(),
                tle=dict(line0=line0, line1=line1, line2=line2),
                azel=[],
                doppler=[],
                timestamps=[],
                setpoints=[],
            )

            azimuth, elevation = orb.get_observer_look(max_el_time, longitude, latitude, altitude)

            overpass_dict["maximum_elevation"] = elevation

            num_secs = 0.0
            while elevation > threshold:
                timestamp = max_el_time - timedelta(seconds=num_secs)

                # calculate the azimuth and elevation for the
                # given timestamp
                azimuth, elevation = orb.get_observer_look(timestamp, longitude, latitude, altitude)

                # extract information for range rate calculation
                sat_pos, sat_vel = orb.get_position(timestamp, normalize=False)
                obs_pos, obs_vel = observer_position(timestamp, longitude, latitude, altitude)

                # calculate the relative poristion of the satellite
                # from the groundstation
                rel_pos = KM_TO_M * (sat_pos - obs_pos)
                rel_vel = KM_TO_M * (sat_vel - obs_vel)

                # if the error between the last is greater than 5 degrees
                # then add it to the overpass list
                if (
                    (len(overpass_dict["azel"]) == 0)
                    or (cosine_error(*overpass_dict["azel"][0], azimuth, elevation) > 5.0)
                    or (elevation < threshold)
                ):
                    overpass_dict["doppler"].insert(
                        0, np.dot(rel_pos, rel_vel) / (np.sqrt(np.dot(rel_pos, rel_pos)) * SPEED_OF_LIGHT)
                    )
                    overpass_dict["azel"].insert(0, [azimuth, elevation])
                    overpass_dict["timestamps"].insert(0, timestamp.isoformat())

                num_secs += 1

            num_secs = 0.0
            azimuth, elevation = overpass_dict["azel"][-1]
            while elevation > threshold:
                timestamp = max_el_time + timedelta(seconds=num_secs)

                # calculate the azimuth and elevation for the
                # given timestamp
                azimuth, elevation = orb.get_observer_look(timestamp, longitude, latitude, altitude)

                # extract information for range rate calculation
                sat_pos, sat_vel = orb.get_position(timestamp, normalize=False)
                obs_pos, obs_vel = observer_position(timestamp, longitude, latitude, altitude)

                # calculate the relative poristion of the satellite
                # from the groundstation
                rel_pos = KM_TO_M * (sat_pos - obs_pos)
                rel_vel = KM_TO_M * (sat_vel - obs_vel)

                # if the error between the last is greater than 5 degrees
                # then add it to the overpass list
                if (
                    (len(overpass_dict["azel"]) == 0)
                    or (cosine_error(*overpass_dict["azel"][-1], azimuth, elevation) > 5.0)
                    or (elevation < threshold)
                ):
                    overpass_dict["doppler"].append(
                        np.dot(rel_pos, rel_vel) / (np.sqrt(np.dot(rel_pos, rel_pos)) * SPEED_OF_LIGHT)
                    )
                    overpass_dict["azel"].append([azimuth, elevation])
                    overpass_dict["timestamps"].append(timestamp.isoformat())

                num_secs += 1

            overpass_list_dict.append(overpass_dict)

            overpass_dict["setpoints"] = get_setpoints(overpass_dict["azel"])

        return overpass_list_dict

    except NotImplementedError:
        return []
