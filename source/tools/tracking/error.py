from math import cos, sin

import numpy as np


def cosine_error(az0, el0, az1, el1):
    """
    gives the cosine error in degrees
    between two az-el setpoints

    Keyword arguments:
    az0 -- azimuth of first point
    el0 -- elevation of first point
    az1 -- azimuth of second point
    el1 -- elevation of second point
    """

    x0 = [
        cos(np.deg2rad(az0)) * cos(np.deg2rad(el0)),
        sin(np.deg2rad(az0)) * cos(np.deg2rad(el0)),
        sin(np.deg2rad(el0)),
    ]

    x1 = [
        cos(np.deg2rad(az1)) * cos(np.deg2rad(el1)),
        sin(np.deg2rad(az1)) * cos(np.deg2rad(el1)),
        sin(np.deg2rad(el1)),
    ]

    return np.rad2deg(np.arccos(np.dot(x0, x1)))
