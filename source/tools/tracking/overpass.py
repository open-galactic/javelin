from pyorbital.orbital import Orbital


def overpass_time_info(query_time, line0, line1, line2, longitude, latitude, altitude, threshold, period):
    """
    returns a list of rise and fall times for
    a leo satellite over a given period

    Keyword arguments:
    query_datetime -- the initial datetime object for the search
    line0 -- tle line 0
    line1 -- tle line 1
    line2 -- tle line 2
    longitude -- the observers longitude
    latitude -- the observers latitude
    altitude -- the observers altidue
    period -- length of the search window for overpasses
    threshold -- degrees above the horizon for satellite overpasses to include
    """

    try:
        # define an orbital object given the tle
        orb = Orbital(line0, line1=line1, line2=line2)

        tle_overpass_list = orb.get_next_passes(
            utc_time=query_time, length=period, lon=longitude, lat=latitude, alt=altitude, horizon=threshold
        )

        return tle_overpass_list

    except NotImplementedError:
        return []
