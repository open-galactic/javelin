import numpy as np
from dateutil.parser import parse
from scipy.interpolate import interp1d

from source.tools.utils import to_abs_local


def refine_overpass(timestamps, data):
    timestamps = np.array([to_abs_local(parse(timestamp)) for timestamp in timestamps])

    f = interp1d(timestamps, data, fill_value="extrapolate")
    timestamps = np.arange(np.min(timestamps) - 15.0, np.max(timestamps) + 15.0, 5.0)
    data = f(timestamps)

    return timestamps, data
