import numpy as np


def get_setpoints(azel):
    """
    takes a set of azimuth and elevation readings
    from an overpass and turns them into a planned path for the
    rotor to follow (minimizing the degrees error)
    """
    azimuth, elevation = np.array(azel).T

    # if the path crosses the north line, then flip the rotor
    # so that it crosses the south pole line instead - this
    # allows for three quadrant tracking

    azel = np.array([azimuth, elevation]).T

    # set the points to a leapfrog path such that the rotor
    # is pointing ahead of the satellite and begind, rather than one
    # or the other.
    azel += np.concatenate([0.5 * np.diff(azel, axis=0), [[0.0, 0.0]]], axis=0)

    return list(azel)
