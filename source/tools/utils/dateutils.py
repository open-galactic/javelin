import time

from dateutil import tz

utc = tz.tzutc()
local = tz.tzlocal()


def to_abs_local(timestamp):
    timestamp = timestamp.replace(tzinfo=utc)
    localtimestamp = timestamp.astimezone(local)
    abstime = time.mktime(localtimestamp.timetuple())

    return abstime
