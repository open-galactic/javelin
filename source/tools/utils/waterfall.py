import matplotlib
import matplotlib.pyplot as plt
import scipy.io.wavfile

matplotlib.use("Agg")


def plot_waterfall(filename):
    samp_rate, signal = scipy.io.wavfile.read(filename)

    signal = signal[:, 0] + 1.0j * signal[:, 1]

    fig = plt.figure(figsize=(10, 4))
    ax = fig.add_subplot(1, 1, 1)

    Pxx, freqs, bins, im = ax.specgram(signal, NFFT=2048, Fs=samp_rate, noverlap=256)

    ax.set_xlabel("time (s)")
    ax.set_ylabel("freq (kHz)")

    ax.set_yticks([-48000, -24000, 0, 24000, 48000])
    ax.set_yticklabels(["-48kHz", "-24kHz", "0kHz", "+24kHz", "+48kHz"])
    # plt.gca().yaxis.set_major_formatter(FormatStrFormatter('%d Hz'))

    plt.tight_layout()
    plt.savefig(filename.split(".")[0] + ".png", dpi=400)
    fig.clear()
    plt.close(fig)
