import pprint
import time
from datetime import datetime

from dateutil.parser import parse

from source.config import altitude, latitude, longitude, threshold
from source.tools.control import GroundstationControl
from source.tools.tracking import Tracker

# create the tracking object for calculating
# overpass and satellite data
tracker = Tracker(longitude, latitude, altitude, threshold)
groundstation = GroundstationControl()


def pull_data():
    tle_list = []
    overpass_data = []

    with open("tle.txt", "r") as f:
        line1, line2 = f.read().splitlines()

        tle_list.append([line1, line2])

    data = tracker.overpass_leapfrog_path(datetime.utcnow(), "", line1, line2)

    overpass_data += data

    overpass_data = sorted(overpass_data, key=lambda overpass: overpass["start_time"])

    return tle_list, overpass_data


# generate initial tle and overpass data
tle_list, overpass_data = pull_data()
next_overpass = overpass_data[0]
print(next_overpass)
track_update = datetime.utcnow()
tracking_sat = False

while True:
    if not tracking_sat:
        info = {
            "current time (UTC)": datetime.utcnow().isoformat(),
            "next overpass": str(parse(next_overpass["start_time"]) - datetime.utcnow()),
            "sat (info)": next_overpass["tle"]["line0"][2:],
            "sat (norad)": int(next_overpass["tle"]["line1"][2:7]),
            "tracking": tracking_sat,
        }
    else:
        info = {
            "current time (UTC)": datetime.utcnow().isoformat(),
            "still visible": str(parse(next_overpass["end_time"]) - datetime.utcnow()),
            "sat (info)": next_overpass["tle"]["line0"][2:],
            "sat (norad)": int(next_overpass["tle"]["line1"][2:7]),
            "tracking": tracking_sat,
        }

    if (parse(next_overpass["start_time"]) - datetime.utcnow()).total_seconds() - 2 * 60 < 0:
        if not tracking_sat:
            norad_id = int(next_overpass["tle"]["line1"][2:7])
            groundstation.command_parser(norad_id, "local", next_overpass)
        tracking_sat = True

    if (parse(next_overpass["end_time"]) - datetime.utcnow()).total_seconds() < 0:
        try:
            tle_list, overpass_data = pull_data()
            next_overpass = overpass_data[0]
        except Exception:
            pass
        track_update = datetime.utcnow()
        tracking_sat = False

    if (datetime.utcnow() - track_update).total_seconds() > 2 * 60:
        if not tracking_sat:
            try:
                tle_list, overpass_data = pull_data()
                next_overpass = overpass_data[0]
            except Exception:
                pass
            track_update = datetime.utcnow()

    time.sleep(10.0)
    pprint.pprint(info, width=1)
